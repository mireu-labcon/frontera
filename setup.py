import setuptools

setuptools.setup(
    author="mireu",
    author_email="alfmalfm1214@gmail.com",
    name="app-frontera",
    version="0.0.2",
    long_description=open("README.md", "r").read(),
    long_description_content_type="text/markdown",
    url="https://github.com/mireu-labcon/frontera",
    description="CMS program basic setting library",
    license='MIT',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
