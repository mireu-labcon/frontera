import frontera
from frontera import *
from flask import Flask, request

app = Flask(__name__)

@app.route("/")
async def main_pages():
    return "Web Program Version : {0}".format(frontera.__version__)

@app.route("/login", methods=["GET", "POST"])
async def login_pages():
    if request.methods == 'GET':
        return "GET"
    elif request.methods == 'POST':
        return "POST"


if __name__ == "__main__":
    app.run("0.0.0.0", "80", debug=True)