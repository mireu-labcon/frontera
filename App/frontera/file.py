import os, markdown, json

setup_read = open(os.getcwd()+"/data/setup/app.json","r",encoding="UTF-8")
file_location = json.load(setup_read)[0]["service_location"]

mdwebfile = ["/data/web/","/data/md/"]

class work:
    def __init__(self, email, name, title):
        self.email = email
        self.name = name
        self.title = title

    def main(data):
        pass
    
    def md_write(self,data):
        for datafile in mdwebfile:
            os.makedirs("{0}{1}{2}".format(file_location,datafile,self.title),exist_ok=True)

        mdfile = open("{1}/data/md/{0}/{0}.md".format(self.title,file_location),'w+',encoding="UTF-8")
        mdfile.write(data)

        webfile = open("{1}/data/web/{0}/{0}.html".format(self.title, file_location),'w+',encoding="UTF-8")
        webfile.write(markdown.markdown(mdfile.read()))
        mdfile.close()
        