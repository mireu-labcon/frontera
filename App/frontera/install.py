import sqlite3, json, os

file_list = ["/data","/data/md","/data/web","/data/setup"]
sqlset = """
        CREATE TABLE `userdb` (
        `id` integer not null primary key autoincrement,
        `user` varchar(255) null,
        `password` varchar(255) null,
        `email` varchar(255) null,
        `phone` varchar(255) null
        );

        CREATE TABLE `file_list` (
        `id` integer not null primary key autoincrement,
        `user` TEXT null,
        `title` TEXT null,
        `file_name` TEXT null,
        `time` datetime not null default CURRENT_TIMESTAMP
        );
        """

def loop(server_name):
    for file in file_list:
        os.mkdir(os.getcwd()+file)

    with open(os.getcwd()+"/file/setup/app.json","w",encoding="UTF-8") as setup_write:
        listdata = [{'server_name':str(server_name), 'install':True, 'service_location':str(os.getcwd()), 'user':{}}]
        json.dump(listdata, setup_write, indent=4)
    
    with sqlite3.connect(os.getcwd()+"/file/setup/frontera.sqlite3") as sqlfile:
        sqlfile.executescript(sqlset)

    return "Basic setup done"

class setup:
    def main(server_name):
        setup_read = open(os.getcwd()+"/file/setup/app.json","w",encoding="UTF-8")
        
        try:
            if json.load(setup_read)[0]["service_location"] == True:
                return "Basic setup already done"
            else:
                loop(server_name)
        except:
            loop(server_name)