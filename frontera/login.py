from .sql import write
import hashlib

class user_info:
    def __init__(self, email, name, phone, password):
        self.email = email
        self.name = name
        self.phone = phone
        self.password = hashlib.sha256(password.encode()).hexdigest()

    def signin(self):
        data = write.user_list()
        for user_data in data:
            if user_data[0] == self.email:
                if user_data[1] == self.password:
                    return self.email
                else:
                    return False
            else:
                return self.email

    def signup(self):
        data = write.user_list()
        for user_data in data:
            if user_data[0] != self.id:
                return write(self.email, self.phone, self.name, self.password).user_add()
            else:
                return False

    def logout(self):
        return False