from .install import setup
import os

__version__ = '0.0.2'

def index():
    if os.path.isfile(os.getcwd()+"/data/setup/app.json") == True:
        pass
    else:
        server_name = input("서비스 이름을 작성 해주십시오 : ")
        try:
            setup.main(server_name)
        except FileNotFoundError:
            raise FileNotFoundError("Administrator Rights Required")

if __name__ == "__main__":
    index()